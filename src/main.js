document.getElementById("var1").innerHTML = 1;
document.getElementById("var2").innerHTML = 2;

const canvas = document.querySelector('canvas');
const c = canvas.getContext('2d');

let url = "http://127.0.0.1:81/data/sample_data.json";
function networkRequest() {
    return fetch(url)
    .then(response => response.json());
}

networkRequest()
    .then(data => document.getElementById("var2").innerHTML = data.numbers.value);

